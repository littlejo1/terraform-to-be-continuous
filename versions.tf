terraform {
  backend "s3" {
    bucket = "terraform-test-to-be-continuous"
    key    = "to-be-continuous"
    region = "us-east-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  required_version = "> 1.2"
}

provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      Terraform = "true"
    }
  }
}
