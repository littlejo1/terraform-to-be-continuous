locals {
  env = terraform.workspace
}


module "ssh" {
  source      = "github.com/littlejo/terraform-aws-security-group?ref=v0.1"
  name        = "ssh-${local.env}"
  description = "access to ssh"
  ingress = [
    {
      description = "ssh"
      port        = 22
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}

